import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/element.js'
//  导入字体图标
import './assets/fonts/iconfont.css'
//  导入全局样式表
import './assets/css/global.css'

import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = 'https://rambuild.cn/api/private/v1/'
// 通过axios请求拦截器添加token，保证拥有获取数据的权限,
// 用use为请求拦截器挂载一个回调，向服务端发数据请求必然在发送请求期间优先调用回调
// 预处理这次请求
// api文档要求，除了login外其他接口请求时候都要携带token，在发请求之前会预处理设置Authorization
axios.interceptors.request.use(config => {
  // console.log(config)
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
Vue.prototype.$http = axios

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
